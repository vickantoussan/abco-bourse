package com.bourse.abcobourse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RetraiteFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RetraiteFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_retraite, container, false)
        val awesomeValidation =  AwesomeValidation(ValidationStyle.BASIC)
        val montant_epargne_retraite = v.findViewById<EditText>(R.id.montant_epargne_retraite)
        val dureedelacotisation_retraite = v.findViewById<EditText>(R.id.dureedelacotisation_retraite)
        val dureedelapension_retraite = v.findViewById<EditText>(R.id.dureedelapension_retraite)
        val  totaldelacotisation_retraite = v.findViewById<EditText>(R.id.totaldelacotisation_retraite)
        val  capitalenfindeperiode_retraite = v.findViewById<EditText>(R.id.capitalenfindeperiode_retraite)
        val  pensionmensuelle_retraite = v.findViewById<EditText>(R.id.pensionmensuelle_retraite)
        val  totalpensionarecevoir_retraite = v.findViewById<EditText>(R.id.totalpensionarecevoir_retraite)
        val  bt_submit_retraite = v.findViewById<Button>(R.id.bt_submit_etude)

        /*awesomeValidation.addValidation(
            this,
            R.id.montant_epargne_retraite,
            RegexTemplate.NOT_EMPTY,
            R.string.montant_epargne_retraite
        )*/

        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RetraiteFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            RetraiteFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}