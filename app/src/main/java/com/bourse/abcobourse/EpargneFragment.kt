package com.bourse.abcobourse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.chaquo.python.Python
import kotlinx.android.synthetic.main.fragment_epargne.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EpargneFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EpargneFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_epargne, container, false)
        val awesomeValidation =  AwesomeValidation(ValidationStyle.BASIC)
        val montant_epargne_etude = v.findViewById<EditText>(R.id.montant_epargne_etude)
        val dureedelacotisation_etude = v.findViewById<EditText>(R.id.dureedelacotisation_etude)
        val dureedelapension_etude = v.findViewById<EditText>(R.id.dureedelapension_etude)
        val  totaldelacotisation_etude = v.findViewById<EditText>(R.id.totaldelacotisation_etude)
        val  capitalenfindeperiode_etude = v.findViewById<EditText>(R.id.capitalenfindeperiode_etude)
        val  pensionmensuelle_etude = v.findViewById<EditText>(R.id.pensionmensuelle_etude)
        val  totalpensionarecevoir_etude = v.findViewById<EditText>(R.id.totalpensionarecevoir_etude)
        val  bt_submit_etude = v.findViewById<Button>(R.id.bt_submit_etude)

        txtPython.text = getHelloPython()

        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EpargneFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            EpargneFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
    private fun getHelloPython():String
    {
        val python = Python.getInstance()
        val pythonFile = python.getModule("hello")
        return pythonFile.callAttr("hellopy", "Victor Kantoussan").toString()
    }
}