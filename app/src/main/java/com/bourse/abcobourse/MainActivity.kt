package com.bourse.abcobourse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{

    lateinit var homeFragment: HomeFragment
    lateinit var epargneFragment: EpargneFragment
    lateinit var retraiteFragment: RetraiteFragment
    lateinit var infoFragment: InfoFragment
    lateinit var configFragment: ConfigFragment

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        homeFragment = HomeFragment.newInstance()
        epargneFragment = EpargneFragment.newInstance()
        retraiteFragment = RetraiteFragment.newInstance()
        infoFragment = InfoFragment.newInstance()
        configFragment = ConfigFragment.newInstance()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.contener, homeFragment)
            .addToBackStack(homeFragment.toString())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_profile -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.contener, homeFragment)
                    .addToBackStack(homeFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                /*val transaction = manager.beginTransaction()
                val fragment =HomeFragment()
                transaction.replace(R.id.toolbar,fragment)
                transaction.addToBackStack(HomeFragment.toString())
                transaction.commit()*/
                //Toast.makeText(this, "Profile clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_messages -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.contener, epargneFragment)
                    .addToBackStack(epargneFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //Toast.makeText(this, "Messages clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_friends -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.contener, retraiteFragment)
                    .addToBackStack(retraiteFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //Toast.makeText(this, "Friends clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_update -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.contener, infoFragment)
                    .addToBackStack(infoFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //Toast.makeText(this, "Update clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_logout -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.contener, configFragment)
                    .addToBackStack(configFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //Toast.makeText(this, "Sign out clicked", Toast.LENGTH_SHORT).show()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}